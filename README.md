# Prerequisits
- python3 installed
- [openAI API Key](https://auth.openai.com/)

# Install
```
python3 -m venv .venv
source .venv/bin/activate
pip install -r requirements.txt
cp ./secret_key.py.template ./secret_key.py
```

# Configure
populate the `secret_key.py` file with proper information

# Start
Each segment of the file `main.py` represents a step.
Each step is meant to run independently. So you uncomment a single segment, and run `python3 basics.py`

# Sources
- [YT: LangChain Crash Course For Beginners | LangChain Tutorial ](https://www.youtube.com/watch?v=nAmC7SoVLd8)
- [YT: Comparing LLMs with LangChain](https://www.youtube.com/watch?v=rFNG0MIEuW0)